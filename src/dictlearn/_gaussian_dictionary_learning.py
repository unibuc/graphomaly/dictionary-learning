import numpy as np

from tqdm import tqdm
from scipy.linalg import norm
from sklearn.base import BaseEstimator
from sklearn.preprocessing import normalize

from ._utils import _normalize_vector, _normalize_columns


def obj_func_template(x, d, y_tilde, sigma, _lambda):
    """
    Objective function template for the nearest Gaussian atom computation.

    Parameters:
        x (numpy.ndarray): Current atom vector.
        d (numpy.ndarray): Selected dictionary atom.
        y_tilde (numpy.ndarray): Residual signal.
        sigma (float): Standard deviation of the Gaussian.
        _lambda (float): Regularization parameter.

    Returns:
        float: Value of the objective function.
    """
    alpha = np.dot(x.T, y_tilde) / np.linalg.norm(x)**2
    return np.linalg.norm(x - d)**2 / sigma**2 + _lambda * np.linalg.norm(y_tilde - alpha * x)**2


def gamma_obj_func_template(gamma, d, y_tilde, obj_func):
    """
    Computes the objective function value for a given gamma.

    Parameters:
        gamma (float): Interpolation coefficient between the dictionary atom and residual.
        d (numpy.ndarray): Selected dictionary atom.
        y_tilde (numpy.ndarray): Residual signal.
        obj_func (callable): Objective function to evaluate.

    Returns:
        float: Objective function value.
    """
    x = gamma * d + (1 - gamma) * y_tilde * np.sign(np.dot(d.T, y_tilde))
    x = x / np.linalg.norm(x)
    return obj_func(x)


def gss(f, a, b, tol=1e-5):
    """
    Golden-section search for finding the minimum of a unimodal function.

    Parameters:
        f (callable): The function to minimize.
        a (float): Lower bound of the interval.
        b (float): Upper bound of the interval.
        tol (float): Tolerance for the stopping condition.

    Returns:
        tuple: Interval [c, d] containing the minimum, with d - c <= tol.
    """
    invphi = (math.sqrt(5) - 1) / 2  # 1 / phi
    invphi2 = (3 - math.sqrt(5)) / 2  # 1 / phi^2

    (a, b) = (min(a, b), max(a, b))
    h = b - a
    if h <= tol:
        return (a, b)

    n = int(math.ceil(math.log(tol / h) / math.log(invphi)))

    c = a + invphi2 * h
    d = a + invphi * h
    yc = f(c)
    yd = f(d)

    for _ in range(n - 1):
        if yc < yd:
            b = d
            d = c
            yd = yc
            h = invphi * h
            c = a + invphi2 * h
            yc = f(c)
        else:
            a = c
            c = d
            yc = yd
            h = invphi * h
            d = a + invphi * h
            yd = f(d)

    return (a, d) if yc < yd else (c, b)


def compute_nearest_uni_norm_dist_atom(y_tilde, d, sigma, _lambda, tol=1e-5):
    """
    Finds the nearest Gaussian atom with a normalized direction.

    Parameters:
        y_tilde (numpy.ndarray): Residual signal.
        d (numpy.ndarray): Selected dictionary atom.
        sigma (float): Standard deviation of the Gaussian.
        _lambda (float): Regularization parameter.
        tol (float): Tolerance for the optimization process.

    Returns:
        tuple: Updated atom vector and corresponding alpha coefficient.
    """
    gamma_left = 0
    gamma_right = 1 - gamma_left

    obj_func = functools.partial(obj_func_template, d=d, y_tilde=y_tilde, sigma=sigma, _lambda=_lambda)
    gamma_func = functools.partial(gamma_obj_func_template, d=d, y_tilde=y_tilde, obj_func=obj_func)
    gamma_left, gamma_right = gss(gamma_func, gamma_left, gamma_right, tol)
    gamma = (gamma_left + gamma_right) / 2

    x = gamma * d + (1 - gamma) * y_tilde * np.sign(np.dot(d.T, y_tilde))
    x = x / np.linalg.norm(x)

    alpha = np.dot(x.T, y_tilde) / np.linalg.norm(x)**2
    return x, alpha


def gauss_omp(y, D, s, sigmas, _lambda=1e-1, _lambda_rate=1.01, max_iter=10, update_lambda=False):
    """
    Gaussian Orthogonal Matching Pursuit (OMP) for sparse coding.

    Parameters:
        y (numpy.ndarray): Input signal to be approximated.
        D (numpy.ndarray): Dictionary matrix with atoms as columns.
        s (int): Sparsity level (number of atoms to use).
        sigmas (numpy.ndarray): Standard deviations of the Gaussian distributions.
        _lambda (float): Initial regularization parameter.
        _lambda_rate (float): Multiplicative rate for updating _lambda.
        max_iter (int): Maximum number of iterations for inner optimization.
        update_lambda (bool): Whether to update _lambda dynamically.

    Returns:
        tuple: 
            - X (numpy.ndarray): Sparse coefficient matrix.
            - alphas (numpy.ndarray): Coefficients for selected atoms.
            - support (list): Indices of selected atoms.
            - errs (numpy.ndarray): Reconstruction error at each iteration.
    """
    support = []
    errs = np.array([])
    error = y

    n_features = D.shape[0]
    X = np.random.randn(n_features, s)
    X = normalize(X, axis=0, norm='l2')

    alphas = np.zeros((s, 1))

    for support_index in range(s):
        norm_error = error
        atoms_direction = np.sign(np.dot(norm_error.T, D))
        distances = np.linalg.norm(atoms_direction * D - norm_error, axis=0)**2 / (2*sigmas**2) + n_features*np.log(sigmas)
        distances[support] = np.inf

        j_atom = np.argmin(distances)
        support.append(j_atom)

        error = error - X[:, support_index:support_index+1] * alphas[support_index]

        for _ in range(max_iter):
            for index, atom_index in enumerate(support):
                x = X[:, index:index + 1]
                d = D[:, atom_index:atom_index + 1]
                alpha = alphas[index]
                sigma = sigmas[atom_index]

                y_tilde = error + alpha * x
                x, alpha = compute_nearest_uni_norm_dist_atom(y_tilde, d, sigma, _lambda)

                X[:, index:index + 1] = x
                alphas[index] = alpha
                error = y_tilde - alpha * x

            if update_lambda:
                _lambda *= _lambda_rate

            error = y - np.dot(X[:, :support_index + 1], alphas[:support_index + 1, :])
            errs = np.append(errs, np.linalg.norm(error))

    x = np.zeros(D.shape[1])
    x[support] = alphas[0]

    return X, alphas, support, errs


def fista(y, D, lam, step_size, x, num_iter):
    """
    Fast Iterative Shrinkage-Thresholding Algorithm (FISTA) for sparse coding initialization.

    Parameters
    ----------
    y : ndarray
        The observed data vector.
    D : ndarray
        The dictionary matrix.
    lam : float
        The regularization parameter (lambda).
    step_size : float
        The step size for the gradient descent.
    x : ndarray
        The initial estimate of the sparse code.
    num_iter : int
        The number of iterations to perform.

    Returns
    -------
    x : ndarray
        The estimated sparse code after optimization.
    """
    x1 = x.copy()
    s1 = 1
    for it in range(num_iter):
        x2, s2 = x1.copy(), s1
        x1 = soft_threshold(x - 2 * step_size * D.T @ (D @ x - y), step_size * lam)
        s1 = (1 + np.sqrt(1 + 4 * s2 ** 2)) / 2
        x = x1 + ((s2 - 1) / s1) * (x1 - x2)
    return x


def soft_threshold(x, threshold):
    """
    Apply the soft thresholding operator to the input data.

    Parameters
    ----------
    x : ndarray
        Input array.
    threshold : float
        Threshold value.

    Returns
    -------
    ndarray
        The result after applying the soft thresholding.
    """
    return np.sign(x) * np.maximum(np.abs(x) - threshold, 0)


def gauss_omp_l1(y, D, sigmas, lam, gam, niter, nit_fista, step_fista):
    """
    Compute sparse representation for a dictionary with Gaussian atoms using L1 regularization.

    This function performs a simplified algorithm that directly uses soft thresholding to update
    the coefficients. FISTA is used only for initialization.

    Parameters
    ----------
    y : ndarray
        The observed data vector.
    D : ndarray
        The dictionary matrix.
    sigmas : float or ndarray
        Standard deviations of the Gaussian atoms. If scalar, it is expanded to an array.
    lam : float
        Regularization parameter for the data fidelity term.
    gam : float
        Regularization parameter for the sparsity term.
    niter : int
        Number of iterations for the outer loop.
    nit_fista : int
        Number of iterations for the FISTA algorithm.
    step_fista : float
        Step size for the FISTA algorithm.

    Returns
    -------
    x : ndarray
        The estimated sparse code.
    Da : ndarray
        The updated dictionary after the algorithm.
    err : ndarray
        Reconstruction error at each iteration.
    proba : ndarray
        Probability values computed during iterations.
    obj : ndarray
        Objective function value at each iteration.
    """
    n = D.shape[1]
    if np.isscalar(sigmas):
        sigmas = sigmas * np.ones(n)
    s2 = 1.0 / (sigmas ** 2)

    # Initialization
    Da = D.copy()
    x = np.zeros(n)
    x = fista(y, Da, gam / lam, step_fista, x, nit_fista)
    obj = np.zeros(niter)
    err = np.zeros(niter)
    proba = np.zeros(niter)

    for it in range(niter):
        r = y - Da @ x  # residual

        # Update actual atoms one by one, in random order
        for j in np.ranom.permutation(n):
            r += Da[:, j] * x[j]  # remove contribution of current atom
            # Update atom and normalize
            Da[:, j] = (1 / (lam * x[j] ** 2 + s2[j])) * (lam * x[j] * r + s2[j] * D[:, j])
            # Update representation coefficient
            x[j] = soft_threshold(Da[:, j] @ r, gam / (2 * lam))
            # Update residual
            r -= Da[:, j] * x[j]

        # Atoms normalization
        Da = _normalize_columns(Da)

        # Compute objective output values
        err[it] = norm(y - Da @ x)
        proba[it] = norm((Da - D) @ np.diag(1 / (sigmas * 2)), 'fro') ** 2
        obj[it] = norm((Da - D) @ np.diag(1 / sigmas), 'fro') ** 2 + lam * norm(y - Da @ x) ** 2 + gam * np.sum(np.abs(x))

    return x, Da, err, proba, obj


def gauss_dl(Y, D, n_nonzero_coefs, sigmas, lam, max_iter):
    """
    Perform Gaussian dictionary learning using Orthogonal Matching Pursuit (OMP).

    Parameters
    ----------
    Y : ndarray
        Data matrix with samples as columns.
    D : ndarray
        Initial dictionary matrix.
    n_nonzero_coefs : int
        Target number of non-zero coefficients in the sparse coding.
    sigmas : float or ndarray
        Standard deviations of the Gaussian atoms. If scalar, it is expanded to an array.
    lam : float
        Regularization parameter (lambda).
    max_iter : int
        Maximum number of iterations to perform.

    Returns
    -------
    D : ndarray
        Learned dictionary matrix.
    X : ndarray
        Sparse codes for the data samples.
    """
    n_components = D.shape[1]
    n_features, n_samples = Y.shape

    for i_iter in tqdm(range(max_iter)):
        D_new = np.zeros((n_features, n_components))
        atoms_counter = np.zeros((1, n_components))

        for i_sample in range(n_samples):
            # Extract the current sample
            y = Y[:, i_sample:i_sample+1]

            # Compute univariate normal distribution
            X, alphas, support, errs = gauss_omp(y, D, sigmas, n_nonzero_coefs, _lambda=lam, max_iter=1)

            # Update D_new atoms
            D_new[:, support] += X
            atoms_counter[:, support] += 1

        # Compute the new dictionary
        idx = np.where(atoms_counter[0, :] != 0)[0]
        D_new[:, idx] /= atoms_counter[0, idx]
        idx = np.where(atoms_counter[0, :] == 0)[0]
        D_new[:, idx] = D[:, idx]
        D = _normalize_columns(D_new)

    return D, X


def gauss_dl_l1(Y, D, sigmas, lam, gam, niter, nit_fista, step_fista):
    """
    Perform dictionary learning with Gaussian atoms and L1 regularization.

    Parameters
    ----------
    Y : ndarray
        Data matrix with samples as columns.
    D : ndarray
        Initial dictionary matrix.
    sigmas : float or ndarray
        Standard deviations of the Gaussian atoms. If scalar, it is expanded to an array.
    lam : float
        Regularization parameter for the data fidelity term.
    gam : float
        Regularization parameter for the sparsity term.
    niter : int
        Number of iterations for the dictionary learning.
    nit_fista : int
        Number of iterations for the FISTA algorithm during sparse coding.
    step_fista : float
        Step size for the FISTA algorithm.

    Returns
    -------
    D : ndarray
        Learned dictionary matrix.
    X : ndarray
        Sparse codes for the data samples.
    """
    m, n = D.shape
    N = Y.shape[1]
    if np.isscalar(sigmas):
        sigmas = sigmas * np.ones(n)
    else:
        sigmas = sigmas.flatten()
    s2 = 1.0 / (sigmas ** 2)

    X = np.zeros((n, N))

    for it in range(niter):
        Dnew = np.zeros((m, n))

        for iN in range(N):
            y = Y[:, iN]
            x = X[:, iN]
            x = fista(y, D, gam / lam, step_fista, x, nit_fista)

            r = y - D @ x  # residual
            Da = D.copy()

            # Update actual atoms one by one, in random order
            for j in np.random.permutation(n):
                r += Da[:, j] * x[j]  # remove contribution of current atom
                # Update atom and normalize
                Da[:, j] = (1 / (lam * x[j] ** 2 + s2[j])) * (lam * x[j] * r + s2[j] * D[:, j])
                # Update representation coefficient
                x[j] = soft_threshold(Da[:, j].T @ r, gam / (2 * lam))
                # Update residual
                r -= Da[:, j] * x[j]

            # Atoms normalization
            Da = _normalize_columns(Da)
            X[:, iN] = x

            support = np.where(x != 0)[0]
            Dnew[:, support] += Da[:, support]

        Dnew = _normalize_columns(Dnew)
        D = Dnew

    return D, X


def gaussian_sparse_encode(Y, D, n_nonzero_coefs, sigmas, lam, method='gauss-omp', **kwargs):
    """
    Compute sparse codes for data samples using a Gaussian sparse encoding method.

    Parameters
    ----------
    Y : ndarray
        Data matrix with samples as columns.
    D : ndarray
        Dictionary matrix.
    n_nonzero_coefs : int
        Target number of non-zero coefficients in the sparse coding.
    sigmas : float or ndarray
        Standard deviations of the Gaussian atoms.
    lam : float
        Regularization parameter (lambda).
    method : str, optional
        Sparse encoding method to use ('gauss-omp' or 'gauss-omp-l1'), by default 'gauss-omp'.
    **kwargs
        Additional keyword arguments to pass to the sparse encoding method.

    Returns
    -------
    err : ndarray
        Reconstruction error for each sample.
    X : ndarray
        Sparse codes for the data samples.
    """
    if method == 'gauss-omp':
        _sparse_encode_method = gauss_omp
    elif method == 'gauss-omp-l1':
        _sparse_encode_method = gauss_omp_l1
    else:
        raise Exception('Unknown Gaussian sparse encode method')

    n_components = D.shape[1]
    n_features, n_samples = Y.shape

    err = np.zeros(n_samples)
    X = np.zeros((n_components, n_samples))

    for idx in range(n_samples):
        y = Y[:, idx]
        x, Da, support = _sparse_encode_method(y, D, n_nonzero_coefs, sigmas, lam, **kwargs)
        err[idx] = np.sqrt(np.linalg.norm(y - Da @ x) ** 2) / np.sqrt(n_features)
        X[support, idx] = x

    return err, X


def gaussian_dictionary_learning(Y, D, n_nonzero_coefs, sigmas, lam, method='gauss-dl', **kwargs):
    """
    Perform Gaussian dictionary learning using the specified method.

    Parameters
    ----------
    Y : ndarray
        Data matrix with samples as columns.
    D : ndarray
        Initial dictionary matrix.
    n_nonzero_coefs : int
        Target number of non-zero coefficients in the sparse coding.
    sigmas : float or ndarray
        Standard deviations of the Gaussian atoms.
    lam : float
        Regularization parameter (lambda).
    method : str, optional
        Dictionary learning method to use ('gauss-dl' or 'gauss-dl-l1'), by default 'gauss-dl'.
    **kwargs
        Additional keyword arguments to pass to the dictionary learning method.

    Returns
    -------
    D : ndarray
        Learned dictionary matrix.
    X : ndarray
        Sparse codes for the data samples.
    """
    if method == 'gauss-dl':
        _dictionary_learning_method = gauss_dl
    elif method == 'gauss-dl-l1':
        _dictionary_learning_method = gauss_dl_l1
    else:
        raise Exception('Unknown Gaussian dictionary learning method')

    D, X = _dictionary_learning_method(Y, D, n_nonzero_coefs, sigmas, lam, **kwargs)

    return D, X



class GaussianDictionaryLearning(BaseEstimator):
    """
    Gaussian Dictionary Learning estimator.

    This class implements Gaussian dictionary learning algorithms for sparse coding,
    using methods such as 'gauss-dl' or 'gauss-dl-l1'.

    Parameters
    ----------
    n_components : int, default=None
        Number of dictionary atoms to extract. If None, then n_components = n_features.

    max_iter : int, default=1000
        Maximum number of iterations to perform (used in 'gauss-dl' algorithm).

    fit_algorithm : {'gauss-dl', 'gauss-dl-l1'}, default='gauss-dl'
        Dictionary learning algorithm to use.

    n_nonzero_coefs : int, default=None
        Target number of non-zero coefficients in the sparse coding (used in 'gauss-dl').

    code_init : ndarray of shape (n_components, n_samples), default=None
        Initial value for the sparse code.

    dict_init : ndarray of shape (n_features, n_components), default=None
        Initial value for the dictionary.

    lam : float, default=1.0
        Regularization parameter (lambda).

    gam : float, default=1.0
        Regularization parameter for the sparsity term (used in 'gauss-dl-l1').

    sigmas : float or ndarray of shape (n_components,), default=None
        Standard deviations of the Gaussian atoms.

    niter : int, default=100
        Number of iterations for the dictionary learning (used in 'gauss-dl-l1').

    nit_fista : int, default=50
        Number of iterations for the FISTA algorithm during sparse coding (used in 'gauss-dl-l1').

    step_fista : float, default=0.1
        Step size for the FISTA algorithm (used in 'gauss-dl-l1').

    verbose : bool, default=False
        Degree of output the procedure will print.

    random_state : int or RandomState instance, default=None
        Determines random number generation for dictionary initialization.

    data_sklearn_compat : bool, default=True
        If True, the input data is expected to have shape (n_samples, n_features).
        If False, the data is expected to have shape (n_features, n_samples).

    **kwargs
        Additional keyword arguments to pass to the dictionary learning method.

    Attributes
    ----------
    D_ : ndarray of shape (n_features, n_components)
        Learned dictionary.

    X_ : ndarray of shape (n_samples, n_components)
        Sparse codes for the data samples.

    errs_ : ndarray
        Reconstruction errors during the learning process.

    probas_ : ndarray
        Probability values computed during the learning process.

    objs_ : ndarray
        Objective function values during the learning process.
    """

    def __init__(
        self,
        n_components=None,
        max_iter=1000,
        fit_algorithm="gauss-dl",
        n_nonzero_coefs=None,
        code_init=None,
        dict_init=None,
        lam=1.0,
        gam=1.0,
        sigmas=None,
        niter=100,
        nit_fista=50,
        step_fista=0.1,
        verbose=False,
        random_state=None,
        data_sklearn_compat=True,
        **kwargs
    ):
        super().__init__()
        self.n_components = n_components
        self.max_iter = max_iter
        self.fit_algorithm = fit_algorithm
        self.n_nonzero_coefs = n_nonzero_coefs
        self.code_init = code_init
        self.dict_init = dict_init
        self.lam = lam
        self.gam = gam
        self.sigmas = sigmas
        self.niter = niter
        self.nit_fista = nit_fista
        self.step_fista = step_fista
        self.verbose = verbose
        self.random_state = random_state
        self.data_sklearn_compat = data_sklearn_compat
        self.kwargs = kwargs

        # Initialize attributes
        self.D_ = None
        self.X_ = None
        self.errs_ = None
        self.probas_ = None
        self.objs_ = None

    def fit(self, Y):
        """
        Fit the model to the data matrix Y.

        Parameters
        ----------
        Y : array-like of shape (n_samples, n_features) or (n_features, n_samples)
            Training data matrix. If `data_sklearn_compat` is True, the data should
            have shape (n_samples, n_features). If False, the data should have shape
            (n_features, n_samples).

        Returns
        -------
        self : object
            Returns the instance itself.
        """
        # Validate data
        Y = check_array(Y)
        if self.data_sklearn_compat:
            Y = Y.T  # Transpose to shape (n_features, n_samples)
        n_features, n_samples = Y.shape

        # Determine number of components
        if self.n_components is None:
            n_components = n_features
        else:
            n_components = self.n_components

        # Initialize dictionary
        if self.dict_init is None:
            rng = np.random.RandomState(self.random_state)
            D_init = rng.randn(n_features, n_components)
            D_init = normalize(D_init, axis=0, norm='l2')
        else:
            D_init = self.dict_init

        # Initialize code
        if self.code_init is None:
            X_init = np.zeros((n_components, n_samples))
        else:
            X_init = self.code_init

        # Set parameters for the method
        method_params = self.kwargs.copy()
        method_params.update({
            'Y': Y,
            'D': D_init,
            'sigmas': self.sigmas,
            'lam': self.lam,
            'method': self.fit_algorithm,
        })

        if self.fit_algorithm == 'gauss-dl':
            method_params['n_nonzero_coefs'] = self.n_nonzero_coefs
            method_params['max_iter'] = self.max_iter
        elif self.fit_algorithm == 'gauss-dl-l1':
            method_params['gam'] = self.gam
            method_params['niter'] = self.niter
            method_params['nit_fista'] = self.nit_fista
            method_params['step_fista'] = self.step_fista
        else:
            raise ValueError("Unknown fit_algorithm: %s" % self.fit_algorithm)

        # Perform dictionary learning
        D, X = gaussian_dictionary_learning(**method_params)

        self.D_ = D
        if self.data_sklearn_compat:
            self.X_ = X.T
        else:
            self.X_ = X

        return self
