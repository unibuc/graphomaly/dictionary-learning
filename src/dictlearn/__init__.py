__version__ = '0.0.1'

from ._dictionary_learning import (
    sparse_encode,
    dictionary_learning,
    kernel_dictionary_learning,
    online_dictionary_learning,
    DictionaryLearning,
    _get_fit_handle
)

from ._gaussian_dictionary_learning import (
    gaussian_sparse_encode,
    gaussian_dictionary_learning
)

from ._cone_dictionary_learning import (
    cone_sparse_encode,
    cone_dictionary_learning,
    check_cone_superposition,
    adjust_cone_radii,
    swap_cone_radii,

    _bisection_method,
    _omp_cone_single,
    disjoin_cones,
)
