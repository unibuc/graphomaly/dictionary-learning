import numpy as np
from joblib import Parallel, delayed

from sklearn.base import BaseEstimator
from sklearn.preprocessing import normalize

from ._utils import _normalize_vector, _normalize_columns
from ._dictionary_learning import dictionary_learning, _get_fit_handle


def _bisection_method(t, r, a, b, tol=1e-6):
    """
    Bisection method to find root.

    Parameters:
        t (float): Ratio parameter.
        r (float): Desired distance.
        a (float): Lower bound of the interval.
        b (float): Upper bound of the interval.
        tol (float): Tolerance for stopping criterion.

    Returns:
        float: Solution found by bisection.
    """
    def f(x):
        return (x * np.sqrt(1 - x**2 * t**2 / 4) +
                x * t * np.sqrt(1 - x**2 / 4) - r)

    # Adjust upper bound if necessary
    if t != 0 and 2 / t < b:
        b = 2 / t

    while abs(b - a) > tol:
        c = (a + b) / 2
        fc = f(c)
        if fc < 0:
            a = c
        else:
            b = c

    return (a + b) / 2


def check_cone_superposition(D, radii, min_dist=0, tol=1e-12):
    """
    Check whether cone atoms have intersections.

    Parameters:
        D (ndarray): Dictionary (normalized atoms) of shape (n_features, n_components).
        radii (float or ndarray): Cone radii (if scalar, then all atoms have the same radius).
        min_dist (float): Minimum distance between cones (default is 0).
        tol (float): Tolerance value (default is 1e-12).

    Returns:
        ndarray: Matrix with 2 columns; each row contains indices of overlapping atoms.
    """
    n_features, n_components = D.shape

    # Ensure radii is an array with the correct length
    if np.isscalar(radii):
        radii = np.full(n_components, radii)
    else:
        radii = np.asarray(radii)
        if radii.size != n_components:
            raise ValueError("Radii vector has the wrong length")

    # Compute the Gram matrix and distances
    G_full = np.abs(D.T @ D)
    G = np.sqrt(2 - 2 * G_full, dtype=complex)

    # Calculate the R matrix
    radii = radii.reshape(-1, 1)
    term1 = radii * np.sqrt(1 - (radii.T * radii) / 4)
    term2 = np.sqrt(1 - (radii * radii.T) / 4) * radii.T
    R = term1 + term2 + min_dist

    # Find indices where G + tol < R (lower triangular part)
    mask = np.tril(G + tol < R, k=-1)
    row_indices, col_indices = np.where(mask)
    s_list = np.column_stack((row_indices, col_indices))
    s_list = s_list[np.lexsort((s_list[:, 0], s_list[:, 1]))] # Used for debugging vs Matlab implementation

    return s_list


def _omp_cone_single(y, D, n_nonzero_coefs, radii, iter_cd=1, err_thresh=1e-14):
    """
    Orthogonal Matching Pursuit with cone atoms.

    Parameters:
        y (ndarray): Signal to represent, shape (n_features,).
        D (ndarray): Dictionary (normalized atoms), shape (n_features, n_components).
        n_nonzero_coefs (int): Sparsity level.
        radii (float or ndarray): Cone radii (if scalar, all atoms have the same radius).
        iter_cd (int): Number of iterations in the coordinate descent refinement.
        err_thresh (float): Representation error threshold for stopping.

    Returns:
        tuple: (x, Da, support) where:
            - x (ndarray): Sparse representation coefficients.
            - Da (ndarray): Dictionary used for the last representation.
            - support (list): Indices of atoms used in representation.
    """
    n_features, n_components = D.shape
    if np.isscalar(radii):
        radii = np.full(n_components, radii)
    radii = np.asarray(radii)
    sq_radii = radii ** 2 / 2
    lam = 1 - sq_radii

    x = np.zeros(n_nonzero_coefs)
    Da = np.zeros((n_features, n_nonzero_coefs))

    r = y.copy()  # Initial residual
    support = []

    for k in range(n_nonzero_coefs):
        rn = r / np.linalg.norm(r)  # Normalized residual

        # Find best current atom
        proj = D.T @ rn
        proj[support] = 0  # Avoid atom duplication
        jmax = np.argmax(lam * np.abs(proj) +
                         np.sqrt((1 - lam ** 2) * (1 - proj ** 2)))

        if proj[jmax] < 0:
            rn = -rn
        p = abs(proj[jmax])

        if p + sq_radii[jmax] >= 1:  # Residual is inside cone
            dc = rn
            res_inside_cone = True
        else:  # Residual outside cone
            b = np.sqrt(1 / (1 - p ** 2))
            q = -b * p * D[:, jmax] + b * rn
            a = 1 - sq_radii[jmax]
            dc = a * D[:, jmax] + np.sqrt(1 - a ** 2) * q
            dc /= np.linalg.norm(dc)
            res_inside_cone = False

        support.append(jmax)

        # Compute (nearly) optimal representation
        Da[:, k] = dc
        if res_inside_cone:
            x[k] = np.dot(dc, r)
            x = x[:k+1]
            Da = Da[:, :k+1]
            return x, Da, support
        else:
            Dk = Da[:, :k+1]
            xk, _, _, _ = np.linalg.lstsq(Dk, y, rcond=None)

            if k > 0:
                for _ in range(iter_cd):
                    r = y - Dk @ xk
                    for i in range(k+1):
                        r += Dk[:, i] * xk[i]
                        rn = r / np.linalg.norm(r)
                        p = np.dot(D[:, support[i]], rn)

                        if p < 0:
                            rn = -rn
                        p = np.abs(p)

                        if p + sq_radii[support[i]] >= 1:
                            Dk[:, i] = rn
                            xk[i] = np.dot(rn, r)
                            x = xk
                            Da = Dk
                            return x, Da, support
                        else:
                            b = np.sqrt(1 / (1 - p ** 2))
                            q = -b * p * D[:, support[i]] + b * rn
                            a = 1 - sq_radii[support[i]]
                            dc = a * D[:, support[i]] + \
                                np.sqrt(1 - a ** 2) * q
                            dc /= np.linalg.norm(dc)
                            Dk[:, i] = dc
                            xk[i] = np.dot(dc, r)
                            r -= dc * xk[i]

        r = y - Dk @ xk
        err = np.linalg.norm(r) / np.linalg.norm(y)
        x[:k+1] = xk
        Da[:, :k+1] = Dk

        if err < err_thresh:
            x = x[:k+1]
            Da = Da[:, :k+1]
            return x, Da, support

    return x, Da, support


def cone_sparse_encode(Y, D, n_nonzero_coefs, radii, iter_cd, err_thresh, n_jobs=1):
    """
    Compute representations of many signals by calling _omp_cone_single on each of them.

    Parameters:
        Y (ndarray): Signals to represent, shape (n_features, n_samples).
        D (ndarray): Dictionary (normalized atoms), shape (n_features, n_components).
        n_nonzero_coefs (int): Sparsity level.
        radii (float or ndarray): Cone radii.
        iter_cd (int): Number of coordinate descent iterations.
        err_thresh (float): Error threshold.
        n_jobs (int): Number of parallel jobs.

    Returns:
        tuple: (X, err) where:
            - X (ndarray): Sparse representations of each signal.
            - err (ndarray): Representation error for each signal.
    """
    n_features, n_samples = Y.shape
    n_components = D.shape[1]
    X = np.zeros((n_components, n_samples))
    err = np.zeros(n_samples)

    # Parallel processing for efficiency
    results = Parallel(n_jobs=n_jobs)(
        delayed(_omp_cone_single)(Y[:, i], D, n_nonzero_coefs, radii, iter_cd, err_thresh)
        for i in range(n_samples)
    )

    for i, (x, Da, supp) in enumerate(results):
        err[i] = np.linalg.norm(Y[:, i] - Da @ x) / np.sqrt(n_features)
        X[supp, i] = x

    return X, err


def adjust_cone_radii(D, radii, min_dist, tol=1e-12):
    """
    Adjust radii such that the minimum distance between atoms is satisfied.

    Parameters:
        D (ndarray): Dictionary (normalized atoms) of shape (n_features, n_components).
        radii (float or ndarray): Cone radii (if scalar, then all atoms have the same radius).
        min_dist (float): Minimum distance between cones.
        tol (float): Tolerance value (default is 1e-12).

    Returns:
        ndarray: Adjusted radii.
    """
    radii = radii.copy()
    s_list = check_cone_superposition(D, radii, min_dist)
    iterations = s_list.shape[0]

    while s_list.size > 0 and iterations > 0:
        i1, i2 = s_list[0]  # Indices of atoms that are too close
        desired_distance = np.sqrt(2 - 2 * abs(np.dot(D[:, i1], D[:, i2]))) - min_dist + tol
        t = radii[i2] / radii[i1]

        # Adjust radii[i1] using bisection method
        radii[i1] = _bisection_method(t, desired_distance, 0, radii[i1])
        radii[i2] = radii[i1] * t
        s_list = check_cone_superposition(D, radii, min_dist)
        iterations -= 1

    if s_list.size > 0:
        print("Remaining overlaps:", s_list)

    return radii


def swap_cone_radii(Y, D, n_nonzero_coefs, radii, min_dist_cones, iter_cd, err_thresh):
    """
    Compute atom usage and swap radii to minimize errors.

    Parameters:
        Y (ndarray): Signals to represent.
        D (ndarray): Dictionary.
        n_nonzero_coefs (int): Sparsity level.
        radii (ndarray): Cone radii.
        min_dist_cones (float): Minimum distance between cones.
        iter_cd (int): Number of coordinate descent iterations.
        err_thresh (float): Error threshold.

    Returns:
        ndarray: Swapped radii.
    """
    X, _ = cone_sparse_encode(Y, D, n_nonzero_coefs, radii, iter_cd, err_thresh)
    atom_use = np.sum(X != 0, axis=1)
    radii_swap = np.zeros_like(radii)

    # Sort indices for swapping
    indices_by_usage = np.argsort(-atom_use)
    indices_by_radii = np.argsort(-radii)
    radii_swap[indices_by_usage] = radii[indices_by_radii]
    radii_swap = adjust_cone_radii(D, radii_swap, min_dist_cones)

    return radii_swap


def disjoin_cones(D, Dup, radii, min_dist=0, tol=1e-12):
    """
    Ensure that cones of the updated dictionary are disjoint by adjusting them towards
    the old dictionary if necessary.

    Parameters:
        D (ndarray): Old dictionary (normalized atoms), shape (n_features, n_components).
        Dup (ndarray): Updated dictionary (normalized atoms), shape (n_features, n_components).
        radii (float or ndarray): Cone radii (scalar or array of length n_components).
        min_dist (float): Minimum distance between cones.
        tol (float): Tolerance value.

    Returns:
        ndarray: Updated dictionary with disjoint atoms.
    """
    Dup = Dup.copy()
    n_features, n_components = D.shape

    if np.isscalar(radii):
        radii = np.full(n_components, radii)
    else:
        radii = np.asarray(radii)
        if radii.size != n_components:
            raise ValueError("Radii vector has the wrong length")

    # Align atoms if they are opposed
    for i in range(n_components):
        if np.dot(D[:, i], Dup[:, i]) < 0:
            Dup[:, i] = -Dup[:, i]

    # Check for cones that are too close
    s_list = check_cone_superposition(Dup, radii, min_dist)
    iteration = 0
    old_conflicts = []

    while s_list.size > 0:
        for i1, i2 in s_list:
            # Align atoms if they are opposed
            if np.dot(D[:, i1], D[:, i2]) < 0:
                D[:, i2] = -D[:, i2]
            if np.dot(Dup[:, i1], Dup[:, i2]) < 0:
                D[:, i2] = -D[:, i2]

            # Calculate distances
            dist_old = np.sqrt(2 - 2 * abs(np.dot(D[:, i1], D[:, i2])))
            dist_new = np.sqrt(2 - 2 * abs(np.dot(Dup[:, i1], Dup[:, i2])))
            desired_dist = (radii[i1] * np.sqrt(1 - (radii[i2] ** 2) / 4) +
                            radii[i2] * np.sqrt(1 - (radii[i1] ** 2) / 4) +
                            min_dist)

            if dist_old + tol < desired_dist:
                if iteration == 0:
                    if dist_new < dist_old:
                        Dup[:, i1] = D[:, i1]
                        Dup[:, i2] = D[:, i2]
                    old_conflicts.append((i1, i2))
            else:
                # Bisection algorithm to adjust atoms
                fi, ff = 0, 1
                d1i, d1f = D[:, i1], Dup[:, i1]
                d2i, d2f = D[:, i2], Dup[:, i2]
                for _ in range(12):
                    f = (fi + ff) / 2
                    d1 = _normalize_vector((1 - f) * d1i + f * d1f)
                    d2 = _normalize_vector((1 - f) * d2i + f * d2f)
                    if np.sqrt(2 - 2 * abs(np.dot(d1, d2))) < desired_dist:
                        ff = f
                        d1f, d2f = d1, d2
                    else:
                        fi = f
                        d1i, d2i = d1, d2
                Dup[:, i1], Dup[:, i2] = d1i, d2i

        # Recheck for conflicts
        s_list = check_cone_superposition(Dup, radii, min_dist)

        if iteration > 10:
            print("Remaining conflicts after maximum iterations:", s_list)
            break

        if s_list.size == 0:
            break

        # Terminate if only old conflicts remain
        if old_conflicts:
            only_old = all(tuple(row) in old_conflicts for row in s_list)
            if only_old:
                break

        iteration += 1

    return Dup


def cone_dictionary_learning(Y, D, n_nonzero_coefs, iternum, radii, iter_cd=1,
                             err_thresh=1e-14, min_d_cones=0, update_quota=1, q_dec_rate=1):
    """
    Cone dictionary learning using a Parallel AK-SVD style, with optimal update of atoms.
    The radii of the cones are not changed.

    Parameters:
        Y (ndarray): Set of signals, shape (n_features, n_samples).
        D (ndarray): Initial dictionary (normalized atoms), shape (n_features, n_components).
        n_nonzero_coefs (int): Sparsity level.
        iternum (int): Number of learning iterations.
        radii (float or ndarray): Cone radii.
        iter_cd (int): Number of iterations in coordinate descent refinement.
        err_thresh (float): Error threshold for OMP.
        min_d_cones (float): Minimum distance between cones.
        update_quota (float): Fraction of atoms updated in one pass (default is 1).
        q_dec_rate (float): Quota decrease rate (update_quota *= q_dec_rate each iteration).

    Returns:
        tuple: (D, X, err) where:
            - D (ndarray): Trained dictionary.
            - X (ndarray): Representation matrix.
            - err (ndarray): Error vector at each iteration.
    """
    n_features, n_components = D.shape
    n_samples = Y.shape[1]
    err = np.zeros(iternum)

    for k in range(iternum):
        n_update = round(update_quota * n_components)
        X = np.zeros((n_components, n_samples))
        Dav = np.zeros((n_features, n_components))
        total_error = 0

        # Select atoms for update if partial update
        if n_update < n_components:
            crt_atoms = np.random.permutation(n_components)[:n_update]

        for i in range(n_samples):
            y = Y[:, i]
            x, Da, support = _omp_cone_single(y, D, n_nonzero_coefs, radii,
                                              iter_cd, err_thresh)
            X[support, i] = x
            r = y - Da @ x  # Representation residual

            # Update dictionary atoms
            if n_update == n_components:
                for j, atom_idx in enumerate(support):
                    f = r + Da[:, j] * x[j]
                    Dav[:, atom_idx] += x[j] * (f - x[j] * (Da[:, j] - D[:, atom_idx]))
            else:
                for j, atom_idx in enumerate(support):
                    if atom_idx in crt_atoms:
                        f = r + Da[:, j] * x[j]
                        Dav[:, atom_idx] += x[j] * (f - x[j] * (Da[:, j] - D[:, atom_idx]))

            total_error += np.linalg.norm(r) ** 2

        err[k] = np.sqrt(total_error) / np.sqrt(n_features * n_samples)

        # Handle cases where new atoms might be zero
        zero_norms = np.linalg.norm(Dav, axis=0) == 0
        Dav[:, zero_norms] = D[:, zero_norms]

        # Update dictionary with disjoint cones
        D = disjoin_cones(D, _normalize_columns(Dav), radii, min_d_cones)

        # Adjust update quota for the next iteration
        update_quota *= q_dec_rate

    return D, X, err


class ConeDictionaryLearning(BaseEstimator):
    """
    Cone Dictionary Learning estimator.

    This class implements dictionary learning algorithms using conic constraints,
    specifically designed to work with data that lies within cone-shaped distributions.

    Parameters
    ----------
    n_components : int, default=None
        Number of dictionary atoms to extract. If None, then n_components = n_features.

    max_iter : int, default=100
        Maximum number of iterations to perform.

    fit_algorithm : {'cone-dl'}, default='cone-dl'
        Dictionary learning algorithm to use.

    n_nonzero_coefs : int, default=None
        Target number of non-zero coefficients in the sparse coding.

    code_init : ndarray of shape (n_components, n_samples), default=None
        Initial value for the sparse code.

    dict_init : ndarray of shape (n_features, n_components), default=None
        Initial value for the dictionary.

    verbose : bool, default=False
        Degree of output the procedure will print.

    random_state : int or RandomState instance, default=None
        Determines random number generation for dictionary initialization.

    params : dict, default=None
        Additional parameters for the fitting algorithm.

    data_sklearn_compat : bool, default=True
        If True, the input data is expected to have shape (n_samples, n_features).
        If False, the data is expected to have shape (n_features, n_samples).

    rad_min : float, default=0.01
        Minimum radius for the cones.

    rad_max : float, default=0.1
        Maximum radius for the cones.

    cd_iter : int, default=10
        Number of iterations for the cone dictionary learning algorithm.

    err_thresh : float, default=1e-7
        Error threshold for convergence.

    min_dist_cones : float, default=0.01
        Minimum distance between cones to ensure disjointness.

    swap_radii : bool, default=True
        Whether to swap radii during initialization to ensure disjoint cones.

    Attributes
    ----------
    D_ : ndarray of shape (n_features, n_components)
        Learned dictionary.

    X_ : ndarray of shape (n_samples, n_components)
        Sparse codes for the data samples.

    err_ : float
        Mean reconstruction error over samples.

    error_ : ndarray
        Reconstruction errors during the learning process.
    """

    def __init__(
        self,
        n_components=None,
        max_iter=100,
        fit_algorithm="cone-dl",
        n_nonzero_coefs=None,
        code_init=None,
        dict_init=None,
        verbose=False,
        random_state=None,
        params=None,
        data_sklearn_compat=True,
        rad_min=0.01,
        rad_max=0.1,
        cd_iter=10,
        err_thresh=1e-7,
        min_dist_cones=0.01,
        swap_radii=True,
    ):
        super().__init__()
        self.n_components = n_components
        self.max_iter = max_iter
        self.fit_algorithm = fit_algorithm
        self.n_nonzero_coefs = n_nonzero_coefs
        self.code_init = code_init
        self.dict_init = dict_init
        self.verbose = verbose
        self.random_state = random_state
        self.params = params or {}
        self.data_sklearn_compat = data_sklearn_compat
        self.rad_min = rad_min
        self.rad_max = rad_max
        self.cd_iter = cd_iter
        self.err_thresh = err_thresh
        self.min_dist_cones = min_dist_cones
        self.swap_radii = swap_radii

        # Initialize attributes
        self.D_ = None
        self.X_ = None
        self.err_ = None
        self.error_ = None

    def _initialize_random_state(self):
        if self.random_state is not None:
            return np.random.default_rng(self.random_state)
        return np.random.default_rng()

    def fit(self, Y):
        """
        Fit the model to the data matrix Y.

        Parameters
        ----------
        Y : array-like of shape (n_samples, n_features) or (n_features, n_samples)
            Training data matrix. If `data_sklearn_compat` is True, the data should
            have shape (n_samples, n_features). If False, the data should have shape
            (n_features, n_samples).

        Returns
        -------
        self : object
            Returns the instance itself.
        """
        # Validate data
        if self.data_sklearn_compat:
            Y = Y.T  # Transpose to shape (n_features, n_samples)
        n_features, n_samples = Y.shape

        # Determine number of components
        if self.n_components is None:
            n_components = n_features
        else:
            n_components = self.n_components

        rng = self._initialize_random_state()

        # Initialize dictionary
        if self.dict_init is None:
            D_init = rng.standard_normal((n_features, n_components))
            D_init = normalize(D_init, axis=0, norm='l2')
        else:
            D_init = self.dict_init

        # Initialize radii
        rad = self.rad_min + (self.rad_max - self.rad_min) * rng.random(n_components)

        # Ensure initial dictionary has disjoint cones
        D_init = self._initialize_dictionary(D_init, rad)

        # Choose fitting algorithm
        if self.fit_algorithm == "cone-dl":
            D, X, error = cone_dictionary_learning(
                Y,
                D_init,
                self.n_nonzero_coefs,
                self.max_iter,
                rad,
                self.cd_iter,
                self.err_thresh,
                self.min_dist_cones,
            )
        else:
            raise ValueError("Unknown fit_algorithm: %s" % self.fit_algorithm)

        # Compute mean reconstruction error
        self.err_ = self._compute_representation_error(Y, D, rad)

        self.D_ = D
        if self.data_sklearn_compat:
            self.X_ = X.T  # Shape (n_samples, n_components)
        else:
            self.X_ = X  # Shape (n_components, n_samples)
        self.error_ = error

        return self

    def _initialize_dictionary(self, D0, rad):
        """
        Ensure the initial dictionary has disjoint cones.

        Parameters
        ----------
        D0 : ndarray of shape (n_features, n_components)
            Initial dictionary matrix.

        rad : ndarray of shape (n_components,)
            Radii for each cone.

        Returns
        -------
        D0 : ndarray of shape (n_features, n_components)
            Adjusted dictionary matrix with disjoint cones.
        """
        i1 = check_cone_superposition(D0, rad, self.min_dist_cones)
        dist_ok = len(i1) == 0
        attempt = 0

        while not dist_ok:
            D0 = normalize(np.random.randn(*D0.shape), axis=0)
            i1 = check_cone_superposition(D0, rad, self.min_dist_cones)
            dist_ok = len(i1) == 0
            attempt += 1
            if attempt == 10 and not dist_ok:
                rad *= 0.95  # Reduce radii to avoid overlap
                attempt = 0
        return D0

    def _compute_representation_error(self, Y, D, rad):
        """
        Compute mean representation error over samples.

        Parameters
        ----------
        Y : ndarray of shape (n_features, n_samples)
            Data matrix.

        D : ndarray of shape (n_features, n_components)
            Dictionary matrix.

        rad : ndarray of shape (n_components,)
            Radii for each cone.

        Returns
        -------
        float
            Mean reconstruction error over samples.
        """
        n_samples = Y.shape[1]
        err = np.zeros(n_samples)

        for k in range(n_samples):
            x, Da, support = _omp_cone_single(
                Y[:, k],
                D,
                self.n_nonzero_coefs,
                rad,
                self.cd_iter,
                self.err_thresh
            )
            err[k] = np.linalg.norm(Y[:, k] - Da @ x)

        return np.mean(err)


if __name__ == '__main__':
    # Define a small 2D dictionary and signals
    D = np.array([[1, 0], [0, 1]])
    Y = np.array([[0.8, 0.2], [0.1, 0.9]])
    n_nonzero_coefs = 1  # sparsity level
    iternum = 3
    rad = 0.5  # cone radius
    iter_cd = 1  # coordinate descent iterations
    err_thresh = 1e-6
    min_dist = 0.1
    update_quota = 1
    q_dec_rate = 0.9
    
    # Run functions in Python
    D_updated, X, err = cone_dictionary_learning(
        Y,
        D, 
        n_nonzero_coefs,
        iternum,
        rad,
        iter_cd,
        err_thresh, 
        min_dist,
        update_quota,
        q_dec_rate
    )
    
    # Display outputs for Python
    print("Python Results:")
    print("Updated Dictionary (D_updated):")
    print(D_updated)
    print("Representation Matrix (X):")
    print(X)
    print("Error Vector (err):")
    print(err)
    
    # Check disjoin_cones function independently
    Dup = np.array([[0.7, 0.1], [0.1, 0.9]])  # slightly perturbed dictionary
    Dup_updated = disjoin_cones(D, Dup, rad, min_dist)
    
    print("Updated Dictionary after disjoin_cones (Dup_updated):")
    print(Dup_updated)
    
    # Run additional tests for _omp_cone_single and check_cone_superposition
    y = np.array([0.8, 0.2])
    x, Da, support = _omp_cone_single(y, D, n_nonzero_coefs, rad, iter_cd, err_thresh)
    
    print("Single representation (x):")
    print(x)
    print("Sub-dictionary used (Da):")
    print(Da)
    print("Support indices (support):")
    print(support)
    
    # Check cone intersections
    s_list = check_cone_superposition(D, rad, min_dist)
    print("List of intersecting cone pairs (s_list):")
    print(s_list)
