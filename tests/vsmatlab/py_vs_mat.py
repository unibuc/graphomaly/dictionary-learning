import scipy.io
import os, sys
import numpy as np
import matplotlib.pyplot as plt

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from omp_matlab import omp_matlab
from nsgk import pnsgk_coh
from _atom import ReplAtoms
from dictionary_learning import dictionary_learning

# define learning method
params = {}
params['replatoms'] = ReplAtoms.NO
learning_method = pnsgk_coh

# read mat file
mat = scipy.io.loadmat('mats/' + learning_method.__name__ + '.mat')

# read Matlab parameters
n_nonzero_coefs = mat['s'][0][0]
n_iterations = mat['K'][0][0]

# read Matlab matrices
Y = mat['Yr']
D0 = mat['D0r']
errs = mat['errs'][0][0]

dictionary, codes, rmse, error_extra = dictionary_learning(Y, D0, 
                                                           n_nonzero_coefs,
                                                           n_iterations,
                                                           omp_matlab,
                                                           learning_method,
                                                           params)

plt.title('err = ' + str(np.linalg.norm(errs - rmse)))
plt.plot(range(n_iterations), errs, label=learning_method.__name__ + '_mat')
plt.plot(range(n_iterations), rmse, label=learning_method.__name__)
plt.legend()
plt.savefig('imgs/' + learning_method.__name__ + '.png')
