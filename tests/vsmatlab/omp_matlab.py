import numpy as np
import matlab.engine
eng = matlab.engine.start_matlab()
eng.addpath('/home/denisilie94/Desktop/dictionary-learning/vsmatlab/omp_matlab/', nargout=0)

def omp_matlab(Y, D, n_nonzero_coefs, params):
    """ Orthogonal Matching Pursuit wrapper """
    Y_mat = matlab.double(Y.tolist())
    D_mat = matlab.double(D.tolist())
    n_nonzero_coefs_mat = matlab.double([[n_nonzero_coefs.tolist()]])
    
    X = np.array(eng.omp(Y_mat, D_mat, n_nonzero_coefs_mat))
    err = (np.linalg.norm(Y - D @ X, 'fro') /
           np.sqrt(Y.size))
    return X, err