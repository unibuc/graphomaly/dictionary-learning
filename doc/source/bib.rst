References
==========

Dumitrescu, B. and Irofti, P., 2018. Dictionary learning algorithms and applications (pp. 1-43). Berlin, Germany: springer.

Zhang, Q. and Li, B., 2010, June. Discriminative K-SVD for dictionary learning in face recognition. In 2010 IEEE computer society conference on computer vision and pattern recognition (pp. 2691-2698). IEEE.

Jiang, Z., Lin, Z. and Davis, L.S., 2013. Label consistent K-SVD: Learning a discriminative dictionary for recognition. IEEE transactions on pattern analysis and machine intelligence, 35(11), pp.2651-2664.

Gu, S., Zhang, L., Zuo, W. and Feng, X., 2014. Projective dictionary pair learning for pattern classification. Advances in neural information processing systems, 27.

Ilie-Ablachim, D.C., Băltoiu, A. and Dumitrescu, B., 2023. Sparse Representation With Gaussian Atoms and Its Use in Anomaly Detection. IEEE Open Journal of Signal Processing.

Ilie-Ablachim, D.C., Baltoiu, A. and Dumitrescu, B., 2023, June. Sparse Representations with Cone Atoms. In ICASSP (pp. 1-5).

Băltoiu, A., Ilie-Ablachim, D.C. and Dumitrescu, B., 2024. Dictionary learning with cone atoms and application to anomaly detection. Signal Processing, 219, p.109398.