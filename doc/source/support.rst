=======
Support
=======

The easiest way to get help with the project is to open an issue on GitLab_.

You can also contact our group via email at Graphomaly_.

.. _Graphomaly: mailto:graphomaly@fmi.unibuc.ro
.. _GitLab: https://gitlab.com/unibuc/graphomaly/dictionary-learning/-/issues
