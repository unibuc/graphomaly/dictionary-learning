Dictionary Learning with Infinite Set Atoms
===========================================

Dictionary Learning with Gaussian Atoms
----------------------------------------
In the Gaussian atoms model, each atom is not represented as a single vector but as a Gaussian distribution centered around a mean vector. Specifically, each atom :math:`\boldsymbol{d}_i` is the mean of a Gaussian distribution :math:`G_i(\boldsymbol{d}_i, \sigma_i)`, defined as:

.. math::

    p(\boldsymbol{a}_i; \boldsymbol{d}_i, \sigma_i) = \frac{1}{\sigma_i^m (2\pi)^{m/2}} \exp\left(-\frac{\|\boldsymbol{a}_i - \boldsymbol{d}_i\|^2}{2\sigma_i^2}\right),

where :math:`\boldsymbol{a}_i` are the vectors used in the representation.

The optimization problem aims to balance two objectives:
    1. Minimizing the representation error.
    2. Maximizing the likelihood of the atoms under the Gaussian distribution.

This is achieved through the following objective function:

.. math::

    \min_{\boldsymbol{a}_i \in \mathbb{R}^m, \boldsymbol{x} \in \mathbb{R}^n} \sum_{x_i \ne 0} \frac{1}{\sigma_i^2} \|\boldsymbol{a}_i - \boldsymbol{d}_i\|^2 + \lambda \|\boldsymbol{y} - \sum_{i=1}^n x_i \boldsymbol{a}_i\|^2

subject to:
    - the sparsity constraint on :math:`\boldsymbol{x}`.
    - the normalization of :math:`\boldsymbol{a}_i`.

Here, :math:`\lambda` is the trade-off parameter between reconstruction error and statistical likelihood.

This approach ensures a sparse code that is both accurate in terms of signal reconstruction and statistically plausible under the Gaussian assumptions.

.. automodule:: dictlearn._gaussian_dictionary_learning
    :members:

Dictionary Learning with Cone Atoms
-----------------------------------
In the cone atoms model, each atom :math:`\boldsymbol{d}_i` defines the center of a cone in the vector space. The set of atoms forms a hypersector of the unit hypersphere. Mathematically, the cone centered at :math:`\boldsymbol{d}_i` with radius :math:`\rho_i` is expressed as:

.. math::

    C(\boldsymbol{d}_i, \rho_i) = \{ \boldsymbol{a} \in \mathbb{R}^m : \|\boldsymbol{a}\| = 1, \|\boldsymbol{a} - \boldsymbol{d}_i\| \leq \rho \}.

The dictionary learning problem for cone atoms involves finding the best coefficients, central atoms, and atoms within the cones to represent the signal :math:`\boldsymbol{y}`. This is formulated as the following optimization problem:

.. math::

    \begin{array}{ccl}
        \underset{\boldsymbol{d}_i \in \mathbb{R}^m, \boldsymbol{x} \in \mathbb{R}^n}{\text{min}}
        & & \left\|\boldsymbol{y} - \sum_{i=1}^n \boldsymbol{a}_{i} x_{i} \right\|_2^2 \\
        \text{s.t.}
        & & \|\boldsymbol{x}\|_{0} \le s, \boldsymbol{a}_{i} \in C(\boldsymbol{d}_i, \rho_i), \ i=1:n
    \end{array}

with constraints:
    - :math:`\|\boldsymbol{x}\|_0 \leq s` (sparsity constraint on :math:`\boldsymbol{x}`).
    - :math:`\boldsymbol{a}_i` must reside in their respective cones.

This model provides a geometrically intuitive approach to sparse representation, where atoms represent volumes in the signal space instead of single points. This added flexibility makes it effective for data with intrinsic variability.

.. automodule:: dictlearn._cone_dictionary_learning
    :members:



