=============
Sparse Coding
=============

.. automodule:: dictlearn.methods._omp
    :members: omp
    :noindex:


.. automodule:: dictlearn.methods._omp
    :members: omp_2d
    :noindex:

.. automodule:: dictlearn.methods._omp
    :members: omp_postreg
    :noindex:

.. automodule:: dictlearn.methods._omp
    :members: ker_omp_postreg
    :noindex: