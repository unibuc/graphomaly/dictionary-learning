.. Dictionary Learning Toolbox documentation master file, created by
   sphinx-quickstart on Sun Aug  8 22:58:12 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Dictionary Learning Toolbox's documentation!
=======================================================

This toolbox solves several Dictionary Learning problems, the basic one being

.. math::

    \begin{align}
    \min _{D, \mathbf{X}} & \|\mathbf{Y}-\mathbf{D} \mathbf{X}\|_{F}^{2} \\
    \text { s.t. } & \left\|\mathbf{x}_{\ell}\right\|_{0} \leq s, \ell=1: N \\
    & \left\|\mathbf{d}_{j}\right\|=1, j=1: n
    \label{eq-dl}
    \end{align}

where :math:`\mathbf{Y} \in \mathbb{R}^{m \times N}` is a given set of :math:`N` signals, each of size :math:`m`, :math:`\mathbf{D} \in \mathbb{R}^{m \times n}` is the trained dictionary, :math:`\mathbf{X} \in \mathbb{R}^{n \times N}` is the sparse representation matrix (or sparse codes) and :math:`s` represents the sparsity level.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   support
   dl_algorithms
   dl_with_infinite_set_atoms
   sparse_coding
   classifiers
   datasets
   bib


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
