========
Datasets
========

AR Face dataset
~~~~~~~~~~~~~~~
.. automodule:: dictlearn.datasets.arface
    :members:

Caltech101 dataset
~~~~~~~~~~~~~~~~~~
.. automodule:: dictlearn.datasets.caltech101
    :members:

MNIST handwritten digits dataset
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. automodule:: dictlearn.datasets.mnist
    :members:

YaleB dataset
~~~~~~~~~~~~~
.. automodule:: dictlearn.datasets.yaleb
    :members: