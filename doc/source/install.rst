============
Installation
============

At the command line

.. code-block:: bash

    easy_install dictlearn

Or, if you have pip installed:

.. code-block:: bash

    pip install dictlearn